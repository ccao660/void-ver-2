using UnityEngine;
using UnityEditor;

namespace Skinner
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(SkinnerParticle))]
    public class SkinnerParticleEditor : Editor
    {
        SerializedProperty _isRendering;
        SerializedProperty _kernelShader;

        SerializedProperty _source;
        SerializedProperty _template;

        SerializedProperty _speedLimit;
        SerializedProperty _drag;
        SerializedProperty _gravity;

        SerializedProperty _minLife;
        SerializedProperty _initialVelocity;

        SerializedProperty _speedToLife;
        SerializedProperty _maxLife;

        SerializedProperty _speedToSpin;
        SerializedProperty _maxSpin;

        SerializedProperty _speedToScale;
        SerializedProperty _minScale;
        SerializedProperty _maxScale;

        SerializedProperty _noiseAmplitude;
        SerializedProperty _noiseFrequency;
        SerializedProperty _noiseMotion;

        SerializedProperty _randomSeed;

        SerializedProperty _forceRadius;
        SerializedProperty _maxForces;
        SerializedProperty _forces;
        SerializedProperty _forceBehavior;
        SerializedProperty _forceMultiplier;
        SerializedProperty _forceDistanceFalloff;
        SerializedProperty _homeAttractorMultiplier;
        SerializedProperty _initialVelocityRandom;

        static GUIContent _labelSpeedToLife = new GUIContent("Life by Speed");
        static GUIContent _labelSpeedToSpin = new GUIContent("Spin by Speed");
        static GUIContent _labelSpeedToScale = new GUIContent("Scale by Speed");
        static GUIContent _labelMinLife = new GUIContent("Emitter Life");
        static GUIContent _labelMaxLife = new GUIContent("Max Life");

        void OnEnable()
        {
            _isRendering = serializedObject.FindProperty("_isRendering");

            _kernelShader = serializedObject.FindProperty("_kernelShader"); 

            _source = serializedObject.FindProperty("_source");
            _template = serializedObject.FindProperty("_template");

            _speedLimit = serializedObject.FindProperty("_speedLimit");
            _drag = serializedObject.FindProperty("_drag");
            _gravity = serializedObject.FindProperty("_gravity");

            _speedToLife = serializedObject.FindProperty("_speedToLife");
            _minLife = serializedObject.FindProperty("_minLife");
            _initialVelocity = serializedObject.FindProperty("_initialVelocity");
            _maxLife = serializedObject.FindProperty("_maxLife");


            _speedToSpin = serializedObject.FindProperty("_speedToSpin");
            _maxSpin = serializedObject.FindProperty("_maxSpin");

            _speedToScale = serializedObject.FindProperty("_speedToScale");
            _minScale = serializedObject.FindProperty("_minScale");
            _maxScale = serializedObject.FindProperty("_maxScale");

            _noiseAmplitude = serializedObject.FindProperty("_noiseAmplitude");
            _noiseFrequency = serializedObject.FindProperty("_noiseFrequency");
            _noiseMotion = serializedObject.FindProperty("_noiseMotion");

            _randomSeed = serializedObject.FindProperty("_randomSeed");

            _forceRadius = serializedObject.FindProperty("_forceRadius");
            _maxForces = serializedObject.FindProperty("_maxForces");
            _forces = serializedObject.FindProperty("_forces");
            _forceBehavior = serializedObject.FindProperty("_forceBehavior");
            _forceMultiplier = serializedObject.FindProperty("_forceMultiplier");
            _forceDistanceFalloff = serializedObject.FindProperty("_forceDistanceFalloff");
            _homeAttractorMultiplier = serializedObject.FindProperty("_homeAttractorMultiplier");
            _initialVelocityRandom = serializedObject.FindProperty("_initialVelocityRandom");

        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            bool reconfigured = false;

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(_isRendering);
            EditorGUILayout.PropertyField(_kernelShader);
            EditorGUILayout.PropertyField(_source);
            EditorGUILayout.PropertyField(_template);
            reconfigured |= EditorGUI.EndChangeCheck();

            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(_speedLimit);
            EditorGUILayout.PropertyField(_drag);
            EditorGUILayout.PropertyField(_gravity);


            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(_minLife, _labelMinLife);
            EditorGUILayout.PropertyField(_initialVelocity);
            EditorGUILayout.PropertyField(_initialVelocityRandom);


            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(_speedToLife, _labelSpeedToLife);
            EditorGUILayout.PropertyField(_maxLife, _labelMaxLife);

            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(_speedToSpin, _labelSpeedToSpin);
            EditorGUILayout.PropertyField(_maxSpin);

            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(_speedToScale, _labelSpeedToScale);
            EditorGUILayout.PropertyField(_minScale);
            EditorGUILayout.PropertyField(_maxScale);

            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(_noiseAmplitude);
            EditorGUILayout.PropertyField(_noiseFrequency);
            EditorGUILayout.PropertyField(_noiseMotion);

            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(_randomSeed);

            EditorGUI.BeginChangeCheck();

            EditorGUILayout.PropertyField(_forceRadius);
            EditorGUILayout.PropertyField(_forceBehavior);
            EditorGUILayout.PropertyField(_forceMultiplier);
            EditorGUILayout.PropertyField(_forceDistanceFalloff);
            EditorGUILayout.PropertyField(_homeAttractorMultiplier);

            EditorGUILayout.PropertyField(_maxForces);

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(_forces, true);

            reconfigured |= EditorGUI.EndChangeCheck();

            if (reconfigured)
                foreach (SkinnerParticle sp in targets) sp.UpdateConfiguration();

            serializedObject.ApplyModifiedProperties();
        }
    }
}
