﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Skinner
{
    /// <summary>
    /// A serializable class holding force 
    /// </summary>
    /// 
    [System.Serializable]
    public class ParticleForce : System.Object
    {

        #region Constructor

        public ParticleForce(Vector3 position, Vector3 speed)
        {
            _position = position;
            _speed = speed;
        }

        #endregion

        #region public properties

        public Vector4 Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }
        [SerializeField]
        Vector4 _position;

        public Vector4 Speed
        {
            get
            {
                return _speed;
            }
            set
            {
                _speed = value;
            }
        }
        [SerializeField]
        Vector4 _speed;




        #endregion


    }
}