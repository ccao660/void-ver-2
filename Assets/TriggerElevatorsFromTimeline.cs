﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TriggerElevatorsFromTimeline : MonoBehaviour {

    public TriggerElevator elevatorTrigger;
    public GameObject lights;

	// Use this for initialization
	void Start () {
        //elevatorTrigger.OpenElevatorDoor();
    }
	


    public void OpenElevators()
    {
        Destroy(lights);
        elevatorTrigger.OpenElevatorDoor();
    }

    public void CloseElevators()
    {
        elevatorTrigger.CloseElevatorDoor();
        StartCoroutine(killElevators());
    }

    public IEnumerator killElevators()
    {
        yield return new WaitForSeconds(15);
          Destroy(elevatorTrigger.gameObject);
    }
}
