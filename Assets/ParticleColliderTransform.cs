﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleColliderTransform : MonoBehaviour {
    public GameObject theTorseRig;
    // Use this for initialization

	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 newTransform = theTorseRig.transform.position;
        transform.position = newTransform;
        transform.rotation = theTorseRig.transform.rotation;
	}
}
