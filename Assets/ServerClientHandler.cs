﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ServerClientHandler : NetworkBehaviour {

    [SyncVar(hook = "OnActiveChange")]
    private bool active = false;
    
    public GameObject SpawnSphere;
    public Transform SphereSpawn;

    private void OnActiveChange(bool updatedActive)
    {
        active = updatedActive;
        SpawnSphere.SetActive(active);
        //bug.Log("Active changed on client:" + active);
    }

    public override void OnStartAuthority()
    {   
        if (hasAuthority)
        {
            GameObject.Find("Canvas").GetComponent<NetworkSpawnClient>().clientCmd = this;
        }
   
    }
    [Command]
    public void CmdTriggerSphere()
    {
        Debug.Log("RUNING CmdTriggerSphere");
        active = !active;
    }


    [ClientRpc]
    public void RpcTriggerSphere()
    {   
        active = !active;
    }


 
}
