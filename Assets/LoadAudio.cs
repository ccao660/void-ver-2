﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LoadAudio : MonoBehaviour {

    public GameObject audioPrefab;
	// Use this for initialization
	void Start () {
        StartCoroutine(loadAudioPrefab());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator loadAudioPrefab()
    {
        yield return new WaitForSeconds(5);
        GameObject.Instantiate(audioPrefab);
    }
}
