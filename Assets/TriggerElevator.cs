﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerElevator : MonoBehaviour {

    public GameObject groundPlane;

    public void Start()
    {
       // OpenElevatorDoor();
    }

    public void OpenElevatorDoor()
    {
        groundPlane.SetActive(true);

        Animator[] animators = GetComponentsInChildren<Animator>();
        for (int k = 0; k < animators.Length; k++)
        {
            animators[k].SetBool("open", true);
        }
    }

    public void CloseElevatorDoor()
    {
        Animator[] animators = GetComponentsInChildren<Animator>();
        for (int k = 0; k < animators.Length; k++)
        {
            animators[k].SetBool("close", true);
        }
    }
}
