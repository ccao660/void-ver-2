﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Skinner;

public class DancerEnabler : MonoBehaviour
{
    public GameObject hairRender;
    public Renderer[] renderObjects;
    public List<float> positions;
    public List<RuntimeAnimatorController> animators;
    public int currentPos = 1;


    private void Awake()
    {
        positions = new List<float>();
        animators = new List<RuntimeAnimatorController>();
    }
    void Start()
    {

        if (renderObjects == null)
        {
           
            renderObjects = gameObject.GetComponentsInChildren<Renderer>();
        }

        foreach (Renderer renderobject in renderObjects)
        {
            renderobject.enabled = false;
        }
    }


    private void Update()
    {
        if (hairRender == null)
        {
            hairRender = transform.Find("Render").gameObject;
            hairRender.SetActive(false);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "VisibleContentCollider")
        {
            foreach (Renderer renderobject in renderObjects)
            {
                if (renderobject.gameObject.name != "SkinnerSource")
                {
                    renderobject.enabled = true;
                }

            }

            //SkinnerConfig skn = gameObject.GetComponentInChildren<SkinnerConfig>();
           // skn.skinnerRenderer.SetActive(true);

            hairRender.SetActive(true);
            Debug.Log("entered viewport " + other.gameObject + " " + transform.position.y);
           
        }
    }

    void OnTriggerExit(Collider other)
    {
     
        if (other.gameObject.tag == "VisibleContentCollider")
        {
            if (currentPos == animators.Count) return;

            gameObject.GetComponent<Animator>().runtimeAnimatorController = animators[currentPos];

            Vector3 lpos = gameObject.transform.localPosition;
            lpos.y = positions[currentPos];
            gameObject.transform.localPosition = lpos;

            foreach (Renderer renderobject in renderObjects)
            {
                    renderobject.enabled = false;
            }
            hairRender.SetActive(false);

            currentPos++;
        }
    }
}
