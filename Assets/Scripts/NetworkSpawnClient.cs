﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;

public class NetworkSpawnClient : MonoBehaviour {

    //[HideInInspector]
    public SpwanClientPrefab player;
    [HideInInspector]
    public TweakMinHeight tweakHeight;
    [HideInInspector]
    public ServerClientHandler clientCmd;
    [HideInInspector]
    public ServerClientHandler serveCmd;

    public void SpawnClientObject()
    {
        Debug.Log("hi " + player);

        player.GetComponent<SpwanClientPrefab>().CmdSpawn();

       
        //tweakHeight.VrikScripts = tweakHeight.localPlayer.GetComponent<VRIK>();
    }

    public void ClientFunction()
    {
       
        clientCmd.GetComponent<ServerClientHandler>().CmdTriggerSphere();
        //clientCmd.GetComponent<ClientCmd>().RpcTriggerSphere();
        Debug.Log("CanvasClinetFunction");
    }


    public void ServerFunction()
    {
        ServerClientHandler[] players = FindObjectsOfType(typeof(ServerClientHandler)) as ServerClientHandler[];
        foreach (ServerClientHandler c in players)
        {
            c.RpcTriggerSphere();
        }
    
    }

}
