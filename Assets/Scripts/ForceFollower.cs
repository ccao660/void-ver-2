﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceFollower : MonoBehaviour {

    public GameObject ToFollow
    {
        get
        {
            return toFollow_;
        }

        set
        {
            toFollow_ = value;
        }
    }
    [SerializeField]
    GameObject toFollow_;


    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {

        if (ToFollow)
        {
            var src = gameObject.transform.position;
            var dst = toFollow_.transform.position;
            var dir = dst - src;
            dst = dst + dir * 10;
            
            dir.Normalize();

            float step = Time.deltaTime;

            transform.position = Vector3.MoveTowards(src, dst, step);
        }

    }
}
