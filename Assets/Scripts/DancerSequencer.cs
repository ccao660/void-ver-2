﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class DancerPosition
{
    public DancerSequencer.DancerID id;
    public float ypos;
    public Animator animator;
    public GameObject obj;
   

    public DancerPosition(DancerSequencer.DancerID id,float ypos,GameObject obj)
    {
        this.id = id;
        this.ypos = ypos;
        this.obj = obj;
    }
}

public class DancerSequencer : MonoBehaviour {

    [Serializable]
    public enum DancerID
    {
        Isabella,
        Sophia,
        Olivia,
        Mia,
        Chloe,
        Alexis,
        Zoe,
        Lily,
        Kaylee,
        Taylor
    };

    public float distanceDelta = 10;
    public float initialDistanceCoeff = 20;
    public int visibledancers = 3;

    //currently dancers repeat every 10 dancers
    public int sceneLength = 10;
    public int currentdancer;

    public List<DancerPosition> fallingDancersPositions;
    public List<DancerPosition> poleDancerPosition;
    public Dancer[] repeatingDancers;
    public GameObject referenceTarget;

    public GameObject fallingDancers;

    public int DebugTime = 0;
    public Animator anim;
    

    public void startWalkSequence()
    {
        anim.SetBool("End", true);
        anim.SetBool("Stop Loop", true);
    }

    public void EndScene()
    {
        Debug.Log("end scene");
        //SceneManager.Instance.changeScene(3);
    }

     public void EnableFallingDancer()
    {
        fallingDancers.gameObject.SetActive(true);
    }

    void Awake()
    {
        Dancer[] fallingDancersList = fallingDancers.GetComponentsInChildren<Dancer>(true);

        foreach (Dancer dancer in fallingDancersList)
        {
            dancer.gameObject.AddComponent<DancerEnabler>();
        }

            Dancer[] findAlldancers = referenceTarget.GetComponentsInChildren<Dancer>(true);
        int k = 0;
        foreach(Dancer dancer in findAlldancers)
        {
            //enablers will only work for 10 dancers
            if (k < 10)
            {
                dancer.gameObject.AddComponent<DancerEnabler>();
                k++;
            }
           
            poleDancerPosition.Add(new DancerPosition(dancer.type, dancer.gameObject.transform.localPosition.y,dancer.gameObject));
            foreach(Dancer repeatingDancer in repeatingDancers)
            {
                if(repeatingDancer.type == dancer.type)
                {
                    repeatingDancer.GetComponent<DancerEnabler>().positions.Add(dancer.gameObject.transform.localPosition.y);
                    repeatingDancer.GetComponent<DancerEnabler>().animators.Add(dancer.gameObject.GetComponent<Animator>().runtimeAnimatorController);
                }
            }
        }
    }
    // Use this for initialization
    void Start () {

        TriggerElevatorsFromTimeline elevatorTrigger = FindObjectOfType<TriggerElevatorsFromTimeline>();
        if(elevatorTrigger != null)
        {
            elevatorTrigger.CloseElevators();
        }

        AudioManager audioManager = FindObjectOfType<AudioManager>();
        audioManager.playAudioTracks();
    }
	
}
