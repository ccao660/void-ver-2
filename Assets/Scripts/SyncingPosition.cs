﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class SyncingPosition : NetworkBehaviour {

	private Transform playerTransform;
	[SerializeField] float lerpRate = 15;
	[SyncVar] private Vector3 syncPos;

	private Vector3 lastPos;
	private float threshold = 0.5f;


	void start(){
		playerTransform = GetComponent<Transform> ();

	}


	void FixedUpdate(){
		TransmitPosition ();
		LerpPosition ();

	}


	void Update()
	{

		Debug.Log ("lastPos" +lastPos);
	}
	void LerpPosition(){

		if (!isLocalPlayer) {

			playerTransform.position = Vector3.Lerp (playerTransform.position, syncPos, Time.deltaTime * lerpRate);

		}

	}



	[Command]
	void Cmd_PorvidePositionToServer(Vector3 pos){
		syncPos = pos;
	}

	[ClientCallback]
	void TransmitPosition(){
		
		if (isLocalPlayer && Vector3.Distance (playerTransform.position, lastPos) > threshold) {

			Cmd_PorvidePositionToServer (playerTransform.position);
			lastPos = playerTransform.position;
		}

	}
}
