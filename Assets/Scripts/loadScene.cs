using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

[Serializable]
public enum SCENE {
	baseScene,
	scene1,
	scene2,
	scene3,
	scene4,
	scene5
}
public class loadScene : NetworkBehaviour {

	public SCENE currentscene;
	public Button[] scenebuttons;
	EventSystem events;
	public GameObject QAMenu;

	bool menuState = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.B)) {

			//Application.LoadLevel (level);
			ChangeScene();
		}
		if (Input.GetKey (KeyCode.A)) {
			Debug.Log (QAMenu.activeInHierarchy);
			if (menuState == QAMenu.activeInHierarchy) {
				QAMenu.SetActive (!menuState);
				menuState = !menuState;
			}
		}

	}

	void ChangeScene()
	{
		Debug.Log ("change scene to " + currentscene.ToString ());
		NetworkManager.singleton.ServerChangeScene(currentscene.ToString());
	}

	public void ChangeScene(int scene) {
		
		string scenename = Enum.GetName (typeof(SCENE), scene);
		currentscene = (SCENE)Enum.Parse (typeof(SCENE), scenename);
		NetworkManager.singleton.ServerChangeScene (scenename);

		for (int i = 0; i < scenebuttons.Length; i++) {
			if (scene-1 == i)
				scenebuttons[i].GetComponent<Image>().color = Color.red;
			else 
				scenebuttons[i].GetComponent<Image>().color = Color.white;
			
		}
	}
}
