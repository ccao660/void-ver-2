﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Skinner;

public class SkinnerLoader : MonoBehaviour {

    public GameObject skinnerRenderer;
    public SkinnerSource ssource;

    // Use this for initialization
    void Start () {
        skinnerRenderer = GameObject.Find("SkinnerRenderer");
        SkinnerParticle debug =  skinnerRenderer.GetComponentInChildren<SkinnerParticle>();
        debug.source = ssource;

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
