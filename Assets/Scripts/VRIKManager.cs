﻿using System.Collections;

using UnityEngine;
using UnityEngine.Networking;
using RootMotion.FinalIK;
#if ENABLE_UNET
using System.Collections.Generic;

public class VRIKManager : NetworkBehaviour
{

    public Animator neuronAnimator;
    public VRIK vrik;
    public bool _isLocalPlayer;
    public GameObject camGlogal;
    //public SteamVR_TrackedObject RightHand;
    //public SteamVR_TrackedObject LeftHand;
    public Vector3 spawnpoint;


    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    void Start () {
        //vrik = GetComponent<VRIK>();

    }
	

	void Update () {
        //Debug.Log(hasAuthority);
        //dont use isLocalPlayer use this hasAuthority if spawn by manul
        if (!hasAuthority)
        {
            //Debug.Log ("not local");
            _isLocalPlayer = false;
            //vrik.enabled = false;
            return;
        }
       


        if (vrik.solver.rightArm.target == null || vrik.solver.leftArm.target == null)
        {
            IKHand[] objs = FindObjectsOfType<IKHand>();

            foreach (IKHand obj in objs)
            {
                //Debug.Log(obj.name);
                if (obj.thishand == IKHand.hands.right)
                {
                    
                    vrik.solver.rightArm.target = obj.transform;
                }

                if (obj.thishand == IKHand.hands.left)
                {
                  
                    vrik.solver.leftArm.target = obj.transform;
                }

            }
        }


        if (camGlogal != null)
        {
            vrik.solver.spine.headTarget = camGlogal.transform;
         
        }

        // in case the cam is still null
        if (camGlogal == null)
        {
            camGlogal = GameObject.Find("NeckHead");
            _isLocalPlayer = true;
        }

        spawnpoint = transform.localPosition;

    }
    //dont use OnStartLocalplayer use this OnStartAuthority() if spawn by manul
    public override void OnStartAuthority()
    {
        vrik.enabled = true;
       // neuronAnimator.enabled = true;
        gameObject.name = "LocalPlayer";
        base.OnStartLocalPlayer();
    }


    public override void OnStartClient()
    {
        Debug.Log("onstart client");
        Debug.Log("I am a host");
    }


    /// <summary>
    /// This is to make server automatically spawn the client
    /// </summary>


    [Command]
    public void CmdRespawn(int charac)
    {

       // GameManager.singleton.VRIKServerRespawn(this, charac);

    }


    [ClientRpc]

    public void RpcChangeSkeleton()
    {

        //ToggleCharacter (false);

        if (isLocalPlayer)
        {

            Transform spawn = NetworkManager.singleton.GetStartPosition();
            //transform.position = spawn.position; 

            transform.position = spawnpoint;
            //transform.rotation = spawn.rotation; 

        }

        Invoke("RespawnSkeleton", 1f);


    }

}
#endif //ENABLE_UNET
