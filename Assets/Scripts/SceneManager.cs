﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class SceneManager : SingletonMonoBehavior<SceneManager> {


    void Start()
    {
      // StartCoroutine(startSceneTest());
    }
	
    public void changeSceneHard(int k)
    {
        Debug.Log("change scene to " + k);
        // UnityEngine.SceneManagement.SceneManager.MergeScenes(UnityEngine.SceneManagement.SceneManager.GetActiveScene(), UnityEngine.SceneManagement.SceneManager.GetSceneByBuildIndex(k));
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }


    IEnumerator  startSceneTest()
    {
        yield return new WaitForSeconds(5);

        changeScene(1);
    }

	public void changeScene(int k) {
        Debug.Log("change scene to " + k);
        AsyncOperation op = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(k, LoadSceneMode.Additive);
    }
}
