﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using System;

[System.Serializable]
public class switch_characters : NetworkBehaviour {


	int currentPlayerIndex;

	[SerializeField]
	public List<GameObject> PlayerObject;
	//public GameObject[] players;



	// Use this for initialization
	void Start () {

		PlayerObject = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {

		if (!isLocalPlayer) {

			return; 

		}

		if (Input.GetKeyUp (KeyCode.W)) {
			CmdchangePlayerObject ();
		}
	}

	[Command]

	void CmdchangePlayerObject(){

			currentPlayerIndex++;

			if (PlayerObject.Count == currentPlayerIndex) {

				currentPlayerIndex = 0;
			}

			foreach ( GameObject gameobject in PlayerObject)
			{

				if (gameobject == PlayerObject [currentPlayerIndex]) {

					gameobject.SetActive (true);

				} else {

					gameobject.SetActive (false);
				}
			}
		}

}
