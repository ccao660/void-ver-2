﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Calibration class allowing input values to be used in different parts of application
/// </summary>
public class Calibration : MonoBehaviour {

	public Vector3 offset;


	public Slider xoffset;
	public Slider yoffset;
	public Slider zoffset;

	public Text[] values;
	// Use this for initialization
	void Start () {

		//go through each input field and map it to the offset;
	
		xoffset.onValueChanged.AddListener (delegate {
			offsetValueChanged (0,xoffset);
		});
		yoffset.onValueChanged.AddListener (delegate {
			offsetValueChanged (1,yoffset);
		});
		zoffset.onValueChanged.AddListener (delegate {
			offsetValueChanged (2,zoffset);
		});

		//offset = new Vector3 (1f, 1f, 1f);

		for (int k = 0; k < values.Length; k++) {
			values [k].text = offset [k].ToString();
		}
		xoffset.value = offset.x;
		yoffset.value = offset.y;
		zoffset.value = offset.z;
	}
	/// <summary>
	/// Offset field value changed handler.
	/// </summary>
	/// <param name="id">Identifier.</param>
	/// <param name="field">Field.</param>
	void offsetValueChanged(int id, Slider field) {
		
		offset [id] = field.value;

		for (int k = 0; k < values.Length; k++) {
			values [k].text = offset [k].ToString ();
		}
	}
}
