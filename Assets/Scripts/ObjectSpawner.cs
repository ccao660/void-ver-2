﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;

[Serializable]
public class objectsToSpawn {

	public Vector3 spawnPosition;
	public GameObject prefab;
	public SCENE belongsToScene;
}

public class ObjectSpawner : NetworkBehaviour {

	public loadScene sceneManager;
	public objectsToSpawn[] networkedObjects;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void OnStartServer() {

		//updateObjectsWithSceneChange ();
	}
		
	public void updateObjectsWithSceneChange() {
		if (isLocalPlayer)
			return;

		foreach (objectsToSpawn objspwn in networkedObjects) {
			if (objspwn.belongsToScene == sceneManager.currentscene) {
				var spawnRotation = Quaternion.Euler(0.0f,0.0f, 0.0f);
				var obj = (GameObject)Instantiate (objspwn.prefab, objspwn.spawnPosition, spawnRotation);
				NetworkServer.Spawn (obj);
			}
		}
	}
}
