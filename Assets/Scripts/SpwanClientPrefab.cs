﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Networking;

public class SpwanClientPrefab : NetworkBehaviour
{
    public GameObject SpwanRefab;
    public GameObject drawingfab;

    // Use this for initialization
    void Start () {

         //find GUI button to spwan the object in net work.
        GameObject.Find("Canvas").GetComponent<NetworkSpawnClient>().player = this;

        if (!isLocalPlayer)
        {
            Debug.Log("not local player abort");
          
        } else
        {
            Debug.Log("local player in spawner");

        }
    }

    void Update()
    {
        //Debug.Log("FirstOne"+isLocalPlayer);
    }

    // this is for GUI spawn Client Prefab
    [Command]
    public void CmdSpawn()
    {
        GameObject go = Instantiate(SpwanRefab);
        NetworkServer.SpawnWithClientAuthority(go, connectionToClient);
        CmdStartDraw();
    }

    // this is for GUI spawn Client Prefab
    [Command]
    public void CmdStartDraw()
    {
        GameObject go = Instantiate(drawingfab);
        NetworkServer.SpawnWithClientAuthority(go, connectionToClient);

   
    }

}
