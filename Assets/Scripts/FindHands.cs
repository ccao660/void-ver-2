﻿using System.Collections;

using UnityEngine;
using UnityEngine.Networking;
using RootMotion.FinalIK;
#if ENABLE_UNET
using System.Collections.Generic;

public class FindHands : NetworkBehaviour
{

    private VRIK vrik;
    public bool _isLocalPlayer;
    public GameObject camGlogal;
    //public SteamVR_TrackedObject RightHand;
    //spublic SteamVR_TrackedObject LeftHand;

    // Use this for initialization
    void Start () {


        vrik = GetComponent<VRIK>();

       // vrik.root;

       // vrik.solver.spine.headTarget
    }
	
	// Update is called once per frame
	void Update () {
        if (!isLocalPlayer)
        {
            //Debug.Log ("not local");
            _isLocalPlayer = false;
            return;
        }
        else
        {
            _isLocalPlayer = true;
        }


        if (vrik.solver.rightArm.target == null || vrik.solver.leftArm.target == null)
        {
            IKHand[] objs = FindObjectsOfType<IKHand>();

            foreach (IKHand obj in objs)
            {
                Debug.Log(obj.name);
                if (obj.thishand == IKHand.hands.right)
                {
                    
                    vrik.solver.rightArm.target = obj.transform;
                }

                if (obj.thishand == IKHand.hands.left)
                {
                  
                    vrik.solver.leftArm.target = obj.transform;
                }

            }
        }


        if (camGlogal != null)
        {
            vrik.solver.spine.headTarget = camGlogal.transform;
         
        }

        // in case the cam is still null
        if (camGlogal == null)
        {
            camGlogal = GameObject.Find("NeckHead"); 
        }
        else
        {

        }

    }
}
#endif //ENABLE_UNET
