﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Skinner;

using GPUTools.Hair.Scripts.Runtime.Render;


public class HandTriggerDancerParticle : MonoBehaviour {


    //this is public so I can see in the editor
    public bool dancerOn = true;
    public bool switchmode = false;

    void OnTriggerEnter(Collider col)
    {
       
        if (col.gameObject.tag == "ParticleCollider")
        {
            Debug.Log("enter");
            if (dancerOn && !switchmode)
            {
                dancerOn = false;
                switchmode = true;
                StartCoroutine(sanityCheck(col));
            }
        }
    }

    void OnTriggerStay(Collider col)
    {
      
        if (col.gameObject.tag == "ParticleCollider")
        {
            dancerOn = false;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "ParticleCollider")
        {
            dancerOn = true;
        }
    }

    IEnumerator sanityCheck(Collider col)
    {
        StartCoroutine(disableDancer(col));
        while (!dancerOn) {
            yield return new WaitForSeconds(3.0f);
        }

        StartCoroutine(enableDancer(col));
        switchmode = false;
    }

    IEnumerator disableDancer(Collider col)
    {

            if (col.gameObject.tag == "ParticleCollider")
            {
                GameObject dissvolerTarget = col.gameObject.transform.parent.gameObject;
                dissvolerTarget.GetComponent<CustomDissolve>().Dissolve();
            
            yield return new WaitForSeconds(0.3f);

            HairRender hr = col.gameObject.transform.parent.gameObject.GetComponentInChildren<HairRender>();
            hr.gameObject.GetComponent<MeshRenderer>().enabled = false;

            SkinnerSource skn = col.gameObject.transform.parent.gameObject.GetComponentInChildren<SkinnerSource>();
            skn.enabled = true;
            skn.gameObject.GetComponent<SkinnerConfig>().skinnerRenderer.SetActive(true);

            yield return new WaitForSeconds(0.2f);
 
         

            Renderer[] renders = col.gameObject.transform.parent.gameObject.GetComponentsInChildren<Renderer>();

            foreach (Renderer render in renders)
            {

                if (render.gameObject.name != "SkinnerSource")
                {
                    render.enabled = false;
                }
            }
        }  
    }


    IEnumerator enableDancer(Collider col)
    {

        if (col.gameObject.tag == "ParticleCollider")
        {
            Debug.Log(col.gameObject.transform.parent.gameObject.name + "__OnTriggerExit");
            
            Renderer[] renders = col.gameObject.transform.parent.gameObject.GetComponentsInChildren<Renderer>();
            foreach (Renderer render in renders)
            {
                if (render.gameObject.name != "SkinnerSource" && render.gameObject.GetComponent<HairRender>() == null)
                {
                    Debug.Log(render.gameObject.name);
                    render.enabled = true;
                }
            }
     
            yield return new WaitForSeconds(0.1f);

            GameObject dissvolerTarget = col.gameObject.transform.parent.gameObject;
            dissvolerTarget.GetComponent<CustomDissolve>().Undissolve();

         
            yield return new WaitForSeconds(0.1f);

            SkinnerSource skn = col.gameObject.transform.parent.gameObject.GetComponentInChildren<SkinnerSource>();
            skn.enabled = false;
            skn.gameObject.GetComponent<SkinnerConfig>().skinnerRenderer.SetActive(false);


            HairRender hr = col.gameObject.transform.parent.gameObject.GetComponentInChildren<HairRender>(true);
            hr.gameObject.GetComponent<MeshRenderer>().enabled = true;

        }

      
    }
        
}