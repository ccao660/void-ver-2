﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableAvatars : MonoBehaviour {

	// Use this for initialization
	void Start () {
        FindObjectOfType<AvatarController>().enableAvatarRenderers();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
