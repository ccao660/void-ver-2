﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {
    
    public List<AudioSource> audioList;
    // Use this for initialization
    void Start () {
     

    }

   public void playAudioTracks()
    {
        foreach(AudioSource track in audioList)
        {
            track.Play();
        }


    }	
	// Update is called once per frame
	void Update () {
		
	}
}
