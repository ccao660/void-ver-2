﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RootMotion.FinalIK;

public class TweakMinHeight : MonoBehaviour {
    public VRIK VrikScripts;
    public GameObject localPlayer;
    // Use this for initialization
    void Start () {
        GameObject.Find("Canvas").GetComponent<NetworkSpawnClient>().tweakHeight = this;
    }
	

    public void TweakTheMinHeight(float newheight)
    {
       localPlayer = GameObject.Find("LocalPlayer");
        //Debug.Log(localPlayer);
        if (localPlayer != null)
        {
            localPlayer.GetComponent<VRIK>().solver.spine.minHeadHeight = newheight;
        }
       
    }

}
