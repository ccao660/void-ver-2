﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DissvolerShaderDebugger : MonoBehaviour
{
    public GameObject target;
    void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, 150, 30), "triggerDissvovler"))
        {

            target.GetComponent<CustomDissolve>().Dissolve();
        }
        if (GUI.Button(new Rect(180, 10, 150, 30), "revovelDissvovler"))
        {
            target.GetComponent<CustomDissolve>().Undissolve();
        }


    }

}