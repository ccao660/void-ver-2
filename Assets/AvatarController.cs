﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Skinner;

public class AvatarController : MonoBehaviour {


    public List<SkinnerParticle> skinnerRenderers;

	public void enableAvatarRenderers()
    {
        foreach(SkinnerParticle sp in skinnerRenderers)
        {
            sp._isRendering = true;
        }
    }
}
