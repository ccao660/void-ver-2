﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Walkoff : MonoBehaviour {

    public Animator anim;
    public Dancer movingDancer;
    public Transform parentobj;

    public void walk()
    {
        Debug.Log("walk dancer");
        movingDancer.transform.parent = parentobj;
        movingDancer.GetComponent<Animator>().runtimeAnimatorController = anim.runtimeAnimatorController;
        movingDancer.GetComponent<Animator>().SetBool("End", true);
        movingDancer.GetComponent<Animator>().SetBool("Stop Loop", true);
    }
}