﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateManagers : MonoBehaviour {
    public GameObject spaceShuttle_0;
    public GameObject spaceShuttle_1;
    public GameObject spaceShuttle_2;
    public GameObject spaceShuttle_3;

    public Transform ShuttleTransform0;
    public Transform ShuttleTransform1;
    public Transform ShuttleTransform2;
    public Transform ShuttleTransform3;

    // Update is called once per frame
    void InstantiateSpaceShuttles() {
        spaceShuttle_0 = (GameObject)Instantiate(Resources.Load("Space_Shuttle"), ShuttleTransform0);
        spaceShuttle_1 = (GameObject)Instantiate(Resources.Load("Space_Shuttle"), ShuttleTransform1);
        spaceShuttle_2 = (GameObject)Instantiate(Resources.Load("Space_Shuttle"), ShuttleTransform2);
        spaceShuttle_3 = (GameObject)Instantiate(Resources.Load("Space_Shuttle"), ShuttleTransform3);
    }

    void DestroySpaceShuttles()
    {
        Destroy(spaceShuttle_0);
        Destroy(spaceShuttle_1);
        Destroy(spaceShuttle_2);
        Destroy(spaceShuttle_3);
    }
}
