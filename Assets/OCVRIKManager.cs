﻿using System.Collections;
using System.Collections.Generic;
using RootMotion.FinalIK;
using UnityEngine;

public class OCVRIKManager : MonoBehaviour {
    public VRIK vrik;

    public GameObject camGlogal;

    void Awake()
    {
      //  DontDestroyOnLoad(transform.gameObject);
    }

    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (camGlogal != null)
        {
            vrik.solver.spine.headTarget = camGlogal.transform;

        }

        // in case the cam is still null
        if (camGlogal == null)
        {
            camGlogal = GameObject.Find("NeckHead");
        }
    }
}
